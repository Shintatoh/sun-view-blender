# Sun View

### Installation

1. Install Blender last version. Can be found at this address: https://www.blender.org/download/
2. Install Sun Position Addon:

    - Download the .zip archive from this link: https://github.com/kevancress/sun_position_b28_test
    - In Blender, go to Edit -> Preferences -> Add-ons -> Install..., and choose the zip folder. When the addon is installed, it must be enabled by ticking the box near its name in the list of installed add-ons.

    ![prefs.png](img/prefs.png)

    ![add-ons.png](img/add-ons.png)

    ![choose-zip.png](img/choose-zip.png)

    ![enable-addon.png](img/enable-addon.png)

3. Install Effin'Art Sun View Addon:
    - Download Sun View from this link: https://bitbucket.org/Shintatoh/sun-view-blender/src/master/
    - Extract the archive.
    - In Blender, Repeat the steps from point **2**, but select *sun-view-blender.py* file instead of the zip archive.

### Usage

1. Import a building: Go to File -> Import, and choose the format of the file. Center the building at the origin if needed.

    ![import-mdl.png](img/import-mdl.png)

2. Configure Sun Position:

    - Make sure the panel *World Properties* is selected.
    - Click on the **Enable** button of panel Sun Position to activate the add-on.

    ![enable-sun-pos-panel.png](img/enable-sun-pos-panel.png)

    - Fill in the light that will be considered as the sun.
    - Enter coordinates and north direction.

    ![sun-position-panel.png](img/sun-position-panel.png)

3. Configure Sun View:
    - Button *Snap Camera to Sun* will synchronize the position and alignment of the default camera with the sun.
    - Set the time frame by editing Start Time and End Time fields. The field *Time Step* controls the intervals between each rendered sun view in hour.
    - The name and location of rendered views can be set using the file selector. The default location is the temporary folder (*/tmp*) for windows. Characters *#* in the name of the file will be replaced by the frame number for images, and the frame interval for videos.
    - Start Sunview button launches the rendering.

    ![sun-view-panel.png](img/sun-view-panel.png)

4. Other Useful Settings:
    - Set camera projection type: Select the camera that will be snapped to the sun. Go to object data panel, choose projection type (Orthogonal, Perspective, Panoramic).
