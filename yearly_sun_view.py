import bpy
import math
import mathutils
import copy

bl_info = \
    {
        "name" : "Sun View Effinart Beta",
        "author" : "Aiulfi Loris <loris.aiufi@effinart.ch>",
        "version" : (1, 0, 0),
        "blender" : (2, 81, 0),
        "location" : "World -> Sun View",
        "description" : "Render views of the scene from the sun position in the specified time interval.",
        "warning" : "Require the addon: Sun Position v28",
        "wiki_url" : "",
        "tracker_url" : "",
        "category" : "Render",
    }

# ------------------------------------------------------------------------
#    Callback Functions
# ------------------------------------------------------------------------

def update_cam_snap(self, context):
    bpy.ops.world.snap_cam()

def set_stamp(scene):
    render = scene.render
    sp = scene.SunPos_property

    render.use_stamp = True
    render.use_stamp_note = True
    render.stamp_note_text = f"Latitude: {sp.Latitude} | Longitude: {sp.Longitude} | UTC: {sp.UTCzone} Date: {sp.Day}.{sp.Month}.{sp.Year} | Time: {sp.Time}"

# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------

class SunViewProperties(bpy.types.PropertyGroup):

    timestep : bpy.props.FloatProperty(
        name = "Timestep",
        description = "Time increment in hour between each rendered sun views.",
        soft_min = 0.01,
        soft_max = 24,
        precision = 3,
        subtype = 'TIME',
        unit = 'TIME',
        default = 1)

    start_h : bpy.props.FloatProperty(
        name = "Start Time",
        description = "Time of the first sun view to render, in hour.",
        soft_min = 0,
        soft_max = 23.999,
        precision = 3,
        subtype = 'TIME',
        unit = 'TIME',
        default = 6)

    end_h : bpy.props.FloatProperty(
        name = "End Time",
        description = "Time of the last sun view to render, in hour.",
        soft_min = 0,
        soft_max = 23.999,
        precision = 3,
        subtype = 'TIME',
        unit = 'TIME',
        default = 21)

    cam_snap : bpy.props.BoolProperty(
        name = "Snap Camera",
        description = "Synchronize the camera to the sun position and orientation.",
        default = False,
        update = update_cam_snap)

    old_cam_pos = mathutils.Vector()
    old_cam_parent = None
    old_cam_rot = mathutils.Euler((0, 0, 0), 'XYZ')

    default_file_format = 'JPEG'

# ------------------------------------------------------------------------
#    Panel
# ------------------------------------------------------------------------

class WORLD_PT_SunViewPanel(bpy.types.Panel):
    bl_idname = "WORLD_PT_sunview_panel"
    bl_label = "Sun View"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "world"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        sv_props = scene.sunview_properties

        box = layout.box()
        box.label(text=" Set the date and location")
        box.label(text=" in the Sun Position panel above")
        box.label(text=" and choose the time frame below.")

        cam_row = layout.row()
        cam_row.prop(sv_props, "cam_snap", text="Snap Camera to Sun", toggle=True)

        box = layout.box()
        box.label(text=" Time Frame:")
        row = box.row(align=True)
        row.prop(sv_props, "start_h", text="Start Time")
        row.prop(sv_props, "end_h", text="End Time")
        row.prop(sv_props, "timestep")

        file_row = layout.row()
        file_row.prop(scene.render, "filepath", text="")

        launch_row = self.layout.row()
        launch_row.operator("world.sun_view", text="Start Sunview")

# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------

class WORLD_OT_SunView(bpy.types.Operator):
    bl_idname = "world.sun_view"
    bl_label = "Run Sun View"

    def execute(self, context):
        scene = context.scene
        sv_props = scene.sunview_properties
        sp_props = scene.SunPos_property

        # Verify start and finish hours
        if self.is_invalid(sv_props.start_h) or self.is_invalid(sv_props.end_h):
            #Display tip
            message = ("Time frame hours are invalid. Must be between 0 and 23.999.")
            self.report({'ERROR'}, message)
            return {"CANCELLED"}

        # Clear existing animation data
        scene.animation_data_clear()

        # Compute the number of frames needed to span the time frame with the given timestep
        step = sv_props.timestep
        start_t = sv_props.start_h
        end_t = sv_props.end_h

        # Setting frame interval of the animation
        start_f = 0
        end_f = math.ceil(math.fabs(end_t - start_t) / step)

        scene.frame_start = start_f
        scene.frame_end = end_f

        # Insert Keyframes with wanted timestep
        curr_t = start_t

        for f in range(start_f, end_f):
            sp_props.Time = curr_t
            sp_props.keyframe_insert("Time", frame = f)

            curr_t = curr_t + step

        sp_props.Time = end_t
        sp_props.keyframe_insert("Time", frame = end_f)

        # If outputing images, the format must be jpeg in
        # order to the metadata to be burned into the image.
        if scene.render.image_settings.file_format == 'PNG':
            scene.render.image_settings.file_format == 'JPEG'

        scene.render.stamp_font_size = 26

        bpy.ops.render.render('INVOKE_DEFAULT', animation=True, write_still=True, use_viewport=True)

        return {"FINISHED"}

    def is_invalid(self, frame_nb):
        return frame_nb < 0 or frame_nb >= 24

class WORLD_OT_SnapCam(bpy.types.Operator):
    bl_idname = "world.snap_cam"
    bl_label = "Snap Camera to Sun"

    def execute(self, context):

        scene = context.scene
        sv_props = scene.sunview_properties
        sp_props = scene.SunPos_property

        if(sv_props.cam_snap):
            sv_props.old_cam_pos.xyz = scene.camera.location
            sv_props.old_cam_parent = scene.camera.parent
            sv_props.old_cam_rot.x = scene.camera.rotation_euler.x
            sv_props.old_cam_rot.y = scene.camera.rotation_euler.y
            sv_props.old_cam_rot.z = scene.camera.rotation_euler.z

            scene.camera.location = mathutils.Vector()
            scene.camera.rotation_euler = mathutils.Euler((0, 0, math.pi), 'XYZ')
            scene.camera.parent = bpy.data.objects[sp_props.SunObject]
        else:
            scene.camera.location = sv_props.old_cam_pos
            scene.camera.parent = sv_props.old_cam_parent
            scene.camera.rotation_euler = sv_props.old_cam_rot

        return {"FINISHED"}

# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    SunViewProperties,
    WORLD_OT_SunView,
    WORLD_OT_SnapCam,
    WORLD_PT_SunViewPanel
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.sunview_properties = bpy.props.PointerProperty(type=SunViewProperties)
    bpy.app.handlers.render_pre.append(set_stamp)

def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.sunview_properties
    bpy.app.handlers.render_pre.remove(set_stamp)

if __name__ == "__main__":
    register()
